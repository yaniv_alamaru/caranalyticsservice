package processor;

import data.CarTimeRecord;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CarTimeRecordProcessor {
    public CarTimeRecordProcessor(int recordsDeltaInMinutes) {
        this.recordsDeltaInMinutes = recordsDeltaInMinutes;
    }
    public int getTotalCars(List<CarTimeRecord> records) {
        return StreamSupport.stream(new CarTimeRecordDecoratorIterator(records, recordsDeltaInMinutes), false)
                .map(CarTimeRecord::getNumberOfCars)
                .reduce(0, Integer::sum);
    }

    public Map<String, Integer> getTotalCarsPerDay(List<CarTimeRecord> records) {
        return StreamSupport.stream(new CarTimeRecordDecoratorIterator(records, recordsDeltaInMinutes), false)
                .collect(Collectors.groupingBy(record -> getDateFormatYYYY_MM_DD(record.getStartTime()),
                Collectors.summingInt(CarTimeRecord::getNumberOfCars)));
    }

    public List<CarTimeRecord> getTopNumberOfCars(List<CarTimeRecord> records, int numberOfTop) {
        return StreamSupport.stream(new CarTimeRecordDecoratorIterator(records, recordsDeltaInMinutes), false)
                .sorted(Comparator.comparingInt(CarTimeRecord::getNumberOfCars).reversed())
                .limit(numberOfTop)
                .collect(Collectors.toList());
    }

    public List<CarTimeRecord> getLeastCarsPeriod(List<CarTimeRecord> records, int windowInUnitOfRecordsDelta) throws IllegalArgumentException {
        verifyRecordsBiggerThenWindowSize(records, windowInUnitOfRecordsDelta);

        CarTimeRecordDecoratorIterator iterator = new CarTimeRecordDecoratorIterator(records, recordsDeltaInMinutes);
        LinkedList<CarTimeRecord> carRecordsInWindow = new LinkedList<>();
        int sum = 0;
        int minimumSum = Integer.MAX_VALUE;
        List<CarTimeRecord> minimumPeriod = carRecordsInWindow;

        while (iterator.tryAdvance(carRecordsInWindow::addLast) &&
               minimumSum > 0) {
            CarTimeRecord newRecord = carRecordsInWindow.getLast();
            sum += newRecord.getNumberOfCars();
            if (carRecordsInWindow.size() == windowInUnitOfRecordsDelta) {
                if (sum <= minimumSum) {
                    minimumSum = sum;
                    minimumPeriod = carRecordsInWindow.stream().collect(Collectors.toList());
                }
                sum -= carRecordsInWindow.removeFirst().getNumberOfCars();
            }
        }

        return minimumPeriod;
    }

    private int pushToQueueAndGetNewSum(LinkedList<CarTimeRecord> carsInWindow, int currentSum, CarTimeRecord newRecord, int windowSize) {
        if (carsInWindow.size() < windowSize) {
            carsInWindow.addLast(newRecord);
            return currentSum + newRecord.getNumberOfCars();
        }
        int newSum = currentSum;
        newSum -= carsInWindow.removeFirst().getNumberOfCars();
        newSum += newRecord.getNumberOfCars();
        carsInWindow.addLast(newRecord);
        return newSum;
    }

    private String getDateFormatYYYY_MM_DD(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    private void verifyRecordsBiggerThenWindowSize(List<CarTimeRecord> records, int windowSize) throws IllegalArgumentException {
        if (records.isEmpty() || windowSize < 1) {
            throw new IllegalArgumentException("Car time record list is empty or smaller than window size is negative");
        }
        long listSizeInMinutes = getListSizeInMinutes(records);
        if (windowSize > 1 &&
                listSizeInMinutes / recordsDeltaInMinutes < windowSize) {
            throw new IllegalArgumentException("Car time record list is smaller than window time requested");
        }
    }

    private long getListSizeInMinutes(List<CarTimeRecord> records) {
        LocalDateTime fromStartTime = records.get(0).getStartTime();
        LocalDateTime toStartTime = records.get(records.size() - 1).getStartTime().plusMinutes(recordsDeltaInMinutes);
        long listSizeInMinutes = ChronoUnit.MINUTES.between(fromStartTime, toStartTime);

        return listSizeInMinutes;
    }

    private int recordsDeltaInMinutes;
}
