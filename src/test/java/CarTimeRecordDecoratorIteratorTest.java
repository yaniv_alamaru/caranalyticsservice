import data.CarTimeRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import processor.CarTimeRecordDecoratorIterator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

public class CarTimeRecordDecoratorIteratorTest {
    CarTimeRecordDecoratorIterator carTimeRecordDecoratorIterator;
    CarTimeRecord firstRecord;
    CarTimeRecord secondRecord;
    CarTimeRecord thirdRecord;

    @BeforeEach
    void setUp() {
        List<CarTimeRecord> records = new ArrayList<>();
        firstRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        secondRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20);
        thirdRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T07:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 30);
        records.add(firstRecord);
        records.add(secondRecord);
        records.add(thirdRecord);
        carTimeRecordDecoratorIterator = new CarTimeRecordDecoratorIterator(records, 30);
    }

    @Test
    void testTryAdvanceForListWithEmptyList() {
        carTimeRecordDecoratorIterator = new CarTimeRecordDecoratorIterator(new ArrayList<>(), 30);
        List<CarTimeRecord> recordList = StreamSupport.stream(carTimeRecordDecoratorIterator, false).collect(Collectors.toList());

        assertEquals(0, recordList.size());
    }

    @Test
    void testTryAdvanceForFirstRealRecord() {
        List<CarTimeRecord> recordList = StreamSupport.stream(carTimeRecordDecoratorIterator, false).limit(1).collect(Collectors.toList());

        assertEquals(1, recordList.size());
        assertEquals(firstRecord, recordList.get(0));
    }

    @Test
    void testTryAdvanceForTwoRealRecords() {
        List<CarTimeRecord> recordList = StreamSupport.stream(carTimeRecordDecoratorIterator, false).limit(2).collect(Collectors.toList());

        assertEquals(2, recordList.size());
        assertEquals(firstRecord, recordList.get(0));
        assertEquals(secondRecord, recordList.get(1));
    }

    @Test
    void testTryAdvanceForTwoRealRecordsAndOneImpliedRecord() {
        List<CarTimeRecord> recordList = StreamSupport.stream(carTimeRecordDecoratorIterator, false).limit(3).collect(Collectors.toList());

        assertEquals(3, recordList.size());
        assertEquals(firstRecord, recordList.get(0));
        assertEquals(secondRecord, recordList.get(1));
        assertEquals(new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 0), recordList.get(2));
    }

    @Test
    void testTryAdvanceForRecordsWithoutLimit() {
        List<CarTimeRecord> recordList = StreamSupport.stream(carTimeRecordDecoratorIterator, false).collect(Collectors.toList());

        assertEquals(4, recordList.size());
        assertEquals(firstRecord, recordList.get(0));
        assertEquals(secondRecord, recordList.get(1));
        assertEquals(new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 0), recordList.get(2));
        assertEquals(thirdRecord, recordList.get(3));
    }
}
