package processor;

import data.CarTimeRecord;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

public class CarTimeRecordDecoratorIterator implements Spliterator<CarTimeRecord> {

    public CarTimeRecordDecoratorIterator(List<CarTimeRecord> records, int recordsDeltaInMinutes) {
        this.records = records;
        this.recordsDeltaInMinutes = recordsDeltaInMinutes;
        index = 0;
        if (!records.isEmpty()) {
            currentRecord = records.get(0);
        }
    }

    @Override
    public boolean tryAdvance(Consumer<? super CarTimeRecord> consumer) {
        if (records.size() <= index) {
            return false;
        }
        if (index == 0) {
            consumer.accept(currentRecord);
            ++index;
        }
        else {
            currentRecord = getNextRecord(currentRecord, records.get(index));
            if (currentRecord.getStartTime().equals(records.get(index).getStartTime())) {
                ++index;
            }
            consumer.accept(currentRecord);
        }
        return true;
    }

    @Override
    public Spliterator<CarTimeRecord> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return records.size() - index;
    }

    @Override
    public int characteristics() {
        return DISTINCT | IMMUTABLE | NONNULL | ORDERED;
    }

    private CarTimeRecord getNextRecord(CarTimeRecord currentRecord, CarTimeRecord nextRecord) {
        LocalDateTime expectedNextRecordStatTime = currentRecord.getStartTime().plusMinutes(recordsDeltaInMinutes);
        if (expectedNextRecordStatTime.equals(nextRecord.getStartTime())) {
            return nextRecord;
        }
        return new CarTimeRecord(expectedNextRecordStatTime, 0);
    }

    private List<CarTimeRecord> records;
    CarTimeRecord currentRecord;
    private int recordsDeltaInMinutes;
    private int index;
}
