import data.CarTimeRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

import Representation.CarTimeRecordStringConverter;

public class CarTimeRecordStringConverterTest {
    CarTimeRecordStringConverter carTimeRecordStringConverter;

    @BeforeEach
    void setUp() {
        carTimeRecordStringConverter = new CarTimeRecordStringConverter();
    }

    @Test
    void TestConvertStringToCarTimeRecord() {

        try {
            CarTimeRecord record = carTimeRecordStringConverter.convert("2016-12-01T05:30:00 12");
            assertEquals(2016, record.getStartTime().getYear());
            assertEquals(Month.DECEMBER, record.getStartTime().getMonth());
            assertEquals(1, record.getStartTime().getDayOfMonth());
            assertEquals(5, record.getStartTime().getHour());
            assertEquals(30, record.getStartTime().getMinute());
            assertEquals(0, record.getStartTime().getSecond());
            assertEquals(12 ,record.getNumberOfCars());
        }
        catch (NumberFormatException e) {
            fail("IOException occured");
        }
    }

    @Test
    void TestConvertCarTimeRecordToString() {
        CarTimeRecord record = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 12);

        assertEquals("2016-12-01T05:30:00 12", carTimeRecordStringConverter.convert(record));
    }
}
