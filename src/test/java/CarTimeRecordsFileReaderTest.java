import data.CarTimeRecord;
import Representation.CarTimeRecordStringConverter;
import Representation.CarTimeRecordsFileReader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CarTimeRecordsFileReaderTest {
    CarTimeRecordsFileReader carTimeRecordsFileReader;
    CarTimeRecordStringConverter carTimeRecordStringConverterMock;
    Path testVectorPath;

    @BeforeEach
    void setUp() throws IOException {
        String testVector = "2016-12-01T05:00:00 5\n" +
                            "2016-12-01T05:30:00 12\n" +
                            "2016-12-01T06:00:00 14";
        testVectorPath = Paths.get("testVector.txt");
        Files.write(testVectorPath, testVector.getBytes());
        carTimeRecordStringConverterMock = mock(CarTimeRecordStringConverter.class);
        carTimeRecordsFileReader = new CarTimeRecordsFileReader(carTimeRecordStringConverterMock);
    }

    @AfterEach
    void tearDown() throws IOException {
        Files.delete(Paths.get("testVector.txt"));
    }

    @Test
    void TestReadRecordsFromFile() throws IOException {
        Collection<CarTimeRecord> result = carTimeRecordsFileReader.read(testVectorPath);

        assertEquals(3, result.size());
        verify(carTimeRecordStringConverterMock, times(1)).convert("2016-12-01T05:00:00 5");
        verify(carTimeRecordStringConverterMock, times(1)).convert("2016-12-01T05:30:00 12");
        verify(carTimeRecordStringConverterMock, times(1)).convert("2016-12-01T06:00:00 14");
    }
}
