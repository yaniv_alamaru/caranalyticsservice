# Car Analytics Service
## Work method
1. Write short methods.
2. Consider performance when implementing methods
3. KISS
4. Utilise best java classes from its class library (Java 8) suitable for the job. 

## Assumptions
1. The input file might have gaps between records, therefore the service assumes that the gaps are records with zero cars. i.e:<br />
   2016-12-01T05:00:00 5<br />
   2016-12-01T06:00:00 14<br />
   It is implies that there is a record  2016-12-01T05:30:00 0

## Design Decisions
1. Separation of data from business logic. Car time records are represented with data classes while string representation, file import, analytics, etc. are represented in separate classes.
2. Creating a custom stream via a splitarator that decorates the car time record collection imported from file with gap records in case of gaps in data (see Assumption 1).
3. Get least cars function is using a queue to store the required window size. The purpose of the queue is to allow efficient way to calculate the window sum. The window sum is maintained by adding to the sum the record added to the queue and subtracting from the sum the record removed from the queue.

## Implementation
1. MainClass to run the assignments and output them to console. 
 
## Testing
1. Test every public method using unit testing.
2. Mock classes that are not simple POJOs.
3. Include testing edge cases.

## Running
set the current directory to CarAnalyticsService<br />
./gradlew build<br />
./gradlew test<br />
java -jar build/libs/CarAnalyticsService-1.0-SNAPSHOT.jar <car time record file><br />

