package data;
import java.time.LocalDateTime;

public class CarTimeRecord {
    private LocalDateTime startTime;
    private int numberOfCars;

    public CarTimeRecord(LocalDateTime startTime, int numberOfCars) {
        this.startTime = startTime;
        this.numberOfCars = numberOfCars;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public int getNumberOfCars() {
        return numberOfCars;
    }

    public void setNumberOfCars(int numberOfCars) {
        this.numberOfCars = numberOfCars;
    }

    public boolean equals(Object o) {
        if (o instanceof CarTimeRecord) {
            CarTimeRecord other = (CarTimeRecord)o;
            return other.numberOfCars == numberOfCars && other.startTime.equals(startTime);
        }
        return false;
    }
}
