import data.CarTimeRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import processor.CarTimeRecordProcessor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class CarTimeRecordProcessorTest {
    CarTimeRecordProcessor carTimeRecordProcessor;

    @BeforeEach
    void setUp() {
        carTimeRecordProcessor = new CarTimeRecordProcessor(30);
    }

    @Test
    void testGetTotalCarsWhenListHasZeroRecords() {
        List<CarTimeRecord> records = new ArrayList<>();

        assertEquals(0, carTimeRecordProcessor.getTotalCars(records));
    }

    @Test
    void testGetTotalCarsWhenListHasOneRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));

        assertEquals(10, carTimeRecordProcessor.getTotalCars(records));
    }

    @Test
    void testGetTotalCarsWhenListHasTwoRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20));

        assertEquals(30, carTimeRecordProcessor.getTotalCars(records));
    }

    @Test
    void testGetTotalCarsPerDayWhenListHasZeroRecords() {
        List<CarTimeRecord> records = new ArrayList<>();
        Map<String, Integer> result = carTimeRecordProcessor.getTotalCarsPerDay(records);

        assertEquals(0, result.size());
    }

    @Test
    void testGetTotalCarsPerDayWhenListHasOneRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));
        Map<String, Integer> result = carTimeRecordProcessor.getTotalCarsPerDay(records);

        assertEquals(1, result.size());
        assertEquals(10, result.get("2016-12-01"));
    }

    @Test
    void testGetTotalCarsPerDayWhenListHasTwoRecordsInSameDay() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20));
        Map<String, Integer> result = carTimeRecordProcessor.getTotalCarsPerDay(records);

        assertEquals(1, result.size());
        assertEquals(30, result.get("2016-12-01"));
    }

    @Test
    void testGetTotalCarsPerDayWhenListHasTwoRecordsInDifferentDays() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-02T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20));
        Map<String, Integer> result = carTimeRecordProcessor.getTotalCarsPerDay(records);

        assertEquals(2, result.size());
        assertEquals(10, result.get("2016-12-01"));
        assertEquals(20, result.get("2016-12-02"));
    }

    @Test
    void testGetTopNumberOfCarsWhenListHasZeroRecords() {
        List<CarTimeRecord> records = new ArrayList<>();

        assertEquals(0, carTimeRecordProcessor.getTopNumberOfCars(records, 3).size());
    }

    @Test
    void testGetOneTopNumberOfCarsWhenListHasOneRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord record = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        records.add(record);
        List<CarTimeRecord> result = carTimeRecordProcessor.getTopNumberOfCars(records, 1);

        assertEquals(1, result.size());
        assertEquals(record, result.get(0));
    }

    @Test
    void testGetTwoTopNumberOfCarsWhenListHasOneRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord record = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        records.add(record);
        List<CarTimeRecord> result = carTimeRecordProcessor.getTopNumberOfCars(records, 2);

        assertEquals(1, result.size());
        assertEquals(record, result.get(0));
    }

    @Test
    void testGetOneTopNumberOfCarsWhenListHasTwoRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord secondTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        CarTimeRecord firstTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20);
        records.add(secondTopRecord);
        records.add(firstTopRecord);
        List<CarTimeRecord> result = carTimeRecordProcessor.getTopNumberOfCars(records, 1);

        assertEquals(1, result.size());
        assertEquals(firstTopRecord, result.get(0));
    }

    @Test
    void testGetTwoTopNumberOfCarsWhenListHasTwoRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord secondTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        CarTimeRecord firstTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20);
        records.add(secondTopRecord);
        records.add(firstTopRecord);
        List<CarTimeRecord> result = carTimeRecordProcessor.getTopNumberOfCars(records, 2);

        assertEquals(2, result.size());
        assertEquals(firstTopRecord, result.get(0));
        assertEquals(secondTopRecord, result.get(1));
    }

    @Test
    void testGetTwoTopNumberOfCarsWhenListHasThreeRecord() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord secondTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        CarTimeRecord firstTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20);
        CarTimeRecord thirdTopRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 5);
        records.add(secondTopRecord);
        records.add(firstTopRecord);
        records.add(thirdTopRecord);
        List<CarTimeRecord> result = carTimeRecordProcessor.getTopNumberOfCars(records, 2);

        assertEquals(2, result.size());
        assertEquals(firstTopRecord, result.get(0));
        assertEquals(secondTopRecord, result.get(1));
    }

    @Test
    void testGetLeastCarsPeriodWhenListHasZeroRecords() {
        List<CarTimeRecord> records = new ArrayList<>();

        assertThrows(IllegalArgumentException.class, () -> carTimeRecordProcessor.getLeastCarsPeriod(records, 1));
    }

    @Test
    void testGetLeastCarsPeriodWhenWindowIsBiggerThanList() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));

        assertThrows(IllegalArgumentException.class, () -> carTimeRecordProcessor.getLeastCarsPeriod(records, 2));
    }

    @Test
    void testGetLeastCarsPeriodWhenWindowIsBiggerThanRecordsDuration() {
        List<CarTimeRecord> records = new ArrayList<>();
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T07:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10));

        assertThrows(IllegalArgumentException.class, () -> carTimeRecordProcessor.getLeastCarsPeriod(records, 5));
    }

    @Test
    void testGetLeastCarsPeriodWhenListHasOneRecordAndWindowIsOne() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord firstRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        records.add(firstRecord);
        List<CarTimeRecord> result = carTimeRecordProcessor.getLeastCarsPeriod(records, 1);

        assertEquals(1, result.size());
        assertEquals(result.get(0), firstRecord);
    }

    @Test
    void testGetLeastCarsPeriodWhenNumnerOfRecordsIsBiggerThenWindow() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord firstRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        records.add(firstRecord);
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20));
        records.add(new CarTimeRecord(LocalDateTime.parse("2016-12-01T07:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 30));
        List<CarTimeRecord> result = carTimeRecordProcessor.getLeastCarsPeriod(records, 2);

        assertEquals(2, result.size());
        assertEquals(result.get(0), firstRecord);
        assertEquals(result.get(1), new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 0));
    }

    @Test
    void testGetLeastCarsPeriodWhenNumberOfRecordsSmallerThenWindow() {
        List<CarTimeRecord> records = new ArrayList<>();
        CarTimeRecord firstRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T05:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 10);
        CarTimeRecord lastRecord = new CarTimeRecord(LocalDateTime.parse("2016-12-01T07:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 20);
        records.add(firstRecord);
        records.add(lastRecord);
        List<CarTimeRecord> result = carTimeRecordProcessor.getLeastCarsPeriod(records, 3);

        assertEquals(3, result.size());
        assertEquals(result.get(0), firstRecord);
        assertEquals(result.get(1), new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:00:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 0));
        assertEquals(result.get(2), new CarTimeRecord(LocalDateTime.parse("2016-12-01T06:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME), 0));
    }
}

