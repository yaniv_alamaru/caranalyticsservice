package Representation;

import data.CarTimeRecord;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class CarTimeRecordsFileReader {
    public CarTimeRecordsFileReader(CarTimeRecordStringConverter carTimeRecordStringConverter) {
        this.carTimeRecordStringConverter = carTimeRecordStringConverter;
    }

    public List<CarTimeRecord> read(Path recordsFilePath) throws IOException {
        return Files.lines(recordsFilePath)
                .map(carTimeRecordStringConverter::convert)
                .collect(Collectors.toList());
    }

    private final CarTimeRecordStringConverter carTimeRecordStringConverter;
}
