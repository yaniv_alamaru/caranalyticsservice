package Representation;

import data.CarTimeRecord;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CarTimeRecordStringConverter {
    public CarTimeRecord convert(String record) throws NumberFormatException {
        String[] pair = record.split(" ");
        String iso8601Date = pair[0];
        int numberOfCars = Integer.parseInt(pair[1]);
        return new CarTimeRecord(LocalDateTime.parse(iso8601Date, DateTimeFormatter.ISO_LOCAL_DATE_TIME), numberOfCars);
    }

    public String convert(CarTimeRecord record) {
        return record.getStartTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) +
                " " +
                record.getNumberOfCars();
    }
}
