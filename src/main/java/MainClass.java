import data.CarTimeRecord;
import Representation.CarTimeRecordStringConverter;
import Representation.CarTimeRecordsFileReader;
import processor.CarTimeRecordProcessor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainClass {
    public static void main(String[] args) {
        ValidateArguments(args);
        CarTimeRecordsFileReader carTimeRecordsFileReader = new CarTimeRecordsFileReader(new CarTimeRecordStringConverter());
        CarTimeRecordProcessor dataProcessor = new CarTimeRecordProcessor(30);
        List<CarTimeRecord> records;

        try {
            records = carTimeRecordsFileReader.read(Paths.get(args[0]));
            System.out.println("Total cars");
            System.out.println(dataProcessor.getTotalCars(records));
            System.out.println("Cars per day");
            Map<String, Integer> totalCarsPerDay = dataProcessor.getTotalCarsPerDay(records);
            totalCarsPerDay.keySet().stream().sorted().forEach(day -> System.out.println(day + " : " + totalCarsPerDay.get(day)));
            System.out.println("3 top number of cars");
            dataProcessor.getTopNumberOfCars(records, 3).forEach(record -> System.out.println((new CarTimeRecordStringConverter()).convert(record)));
            System.out.println("Least cars in 1.5 hours");
            dataProcessor.getLeastCarsPeriod(records, 3).forEach(record -> System.out.println((new CarTimeRecordStringConverter()).convert(record)));
        } catch (IOException e) {
            System.out.println("Unexpected error");
            System.exit(-3);
        }
    }

    public static void ValidateArguments(String[] args) {
        if (args.length != 1) {
            System.out.println("Missing parameters");
            Usage();
            System.exit(-1);
        }

        Path path = Paths.get(args[0]);
        if (!Files.exists(path)) {
            System.out.println("Input file does not exist");
            Usage();
            System.exit(-2);
        }
    }

    public static void Usage() {
        System.out.print("CarAnalyticsService <input car time records file>");
    }
}
